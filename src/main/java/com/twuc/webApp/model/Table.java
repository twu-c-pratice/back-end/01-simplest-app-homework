package com.twuc.webApp.model;

import java.util.function.BiFunction;

public abstract class Table {
    private int num;
    private String operator;
    private BiFunction<Integer, Integer, Integer> operationMethod;

    Table(int num, OperationEnum operationEnum) {
        this.num = num;
        this.operator = operationEnum.getOperator();
        this.operationMethod = operationEnum.getOperationMethod();
    }

    Table(OperationEnum operationEnum) {
        this(9, operationEnum);
    }

    public String getTableString() {
        StringBuilder result = new StringBuilder();
        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                int operateResult = operationMethod.apply(i, j);
                result.append(
                        String.valueOf(j)
                                + " "
                                + operator
                                + " "
                                + String.valueOf(i)
                                + " = "
                                + String.valueOf(operateResult)
                                + "\t");
            }
            result.append("\n");
        }
        return result.toString();
    }
}
