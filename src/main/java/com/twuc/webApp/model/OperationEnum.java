package com.twuc.webApp.model;

import java.util.function.BiFunction;

public enum OperationEnum {
    PLUS("+", (Integer a, Integer b) -> a + b),
    MULTIPLY("*", (Integer a, Integer b) -> a * b);

    private String operator;
    private BiFunction<Integer, Integer, Integer> operationMethod;

    OperationEnum(String operator, BiFunction<Integer, Integer, Integer> operationMethod) {
        this.operator = operator;
        this.operationMethod = operationMethod;
    }

    public String getOperator() {
        return operator;
    }

    public BiFunction<Integer, Integer, Integer> getOperationMethod() {
        return operationMethod;
    }
}
