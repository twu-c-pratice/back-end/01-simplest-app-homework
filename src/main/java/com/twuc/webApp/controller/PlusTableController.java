package com.twuc.webApp.controller;

import com.twuc.webApp.model.PlusTable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlusTableController {
    @GetMapping("/api/tables/plus")
    String getPlusTableString() {
        PlusTable plusTable = new PlusTable();
        return plusTable.getTableString();
    }
}
