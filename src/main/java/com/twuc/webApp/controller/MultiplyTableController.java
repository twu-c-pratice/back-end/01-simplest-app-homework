package com.twuc.webApp.controller;

import com.twuc.webApp.model.MultiplyTable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MultiplyTableController {
    @GetMapping("/api/tables/multiply")
    String getMultiplyTableString() {
        MultiplyTable multiplyTable = new MultiplyTable();
        return multiplyTable.getTableString();
    }
}
