package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class MultiplyTableIntegrationTest {
    @Autowired private MockMvc mockMvc;

    @Test
    void should_return_state_code_with_multiply_table_string_given_request() throws Exception {
        String expectTableString =
                "1 * 1 = 1\t\n"
                        + "1 * 2 = 2\t2 * 2 = 4\t\n"
                        + "1 * 3 = 3\t2 * 3 = 6\t3 * 3 = 9\t\n"
                        + "1 * 4 = 4\t2 * 4 = 8\t3 * 4 = 12\t4 * 4 = 16\t\n"
                        + "1 * 5 = 5\t2 * 5 = 10\t3 * 5 = 15\t4 * 5 = 20\t5 * 5 = 25\t\n"
                        + "1 * 6 = 6\t2 * 6 = 12\t3 * 6 = 18\t4 * 6 = 24\t5 * 6 = 30\t6 * 6 = 36\t\n"
                        + "1 * 7 = 7\t2 * 7 = 14\t3 * 7 = 21\t4 * 7 = 28\t5 * 7 = 35\t6 * 7 = 42\t7 * 7 = 49\t\n"
                        + "1 * 8 = 8\t2 * 8 = 16\t3 * 8 = 24\t4 * 8 = 32\t5 * 8 = 40\t6 * 8 = 48\t7 * 8 = 56\t8 * 8 = 64\t\n"
                        + "1 * 9 = 9\t2 * 9 = 18\t3 * 9 = 27\t4 * 9 = 36\t5 * 9 = 45\t6 * 9 = 54\t7 * 9 = 63\t8 * 9 = 72\t9 * 9 = 81\t\n";

        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/multiply"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.content().string(expectTableString));
        ;
    }
}
