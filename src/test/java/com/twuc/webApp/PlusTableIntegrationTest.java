package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class PlusTableIntegrationTest {
    @Autowired private MockMvc mockMvc;

    @Test
    void should_return_state_code_with_plus_table_string_given_request() throws Exception {
        String expectTableString =
                "1 + 1 = 2\t\n1 + 2 = 3\t2 + 2 = 4\t\n1 + 3 = 4\t2 + 3 = 5\t3 + 3 = 6\t\n1 + 4 = 5	2 + 4 = 6	3 + 4 = 7	4 + 4 = 8\t\n1 + 5 = 6	2 + 5 = 7	3 + 5 = 8	4 + 5 = 9	5 + 5 = 10\t\n1 + 6 = 7	2 + 6 = 8	3 + 6 = 9	4 + 6 = 10	5 + 6 = 11	6 + 6 = 12\t\n1 + 7 = 8	2 + 7 = 9	3 + 7 = 10	4 + 7 = 11	5 + 7 = 12	6 + 7 = 13	7 + 7 = 14\t\n1 + 8 = 9	2 + 8 = 10	3 + 8 = 11	4 + 8 = 12	5 + 8 = 13	6 + 8 = 14	7 + 8 = 15	8 + 8 = 16\t\n1 + 9 = 10	2 + 9 = 11	3 + 9 = 12	4 + 9 = 13	5 + 9 = 14	6 + 9 = 15	7 + 9 = 16	8 + 9 = 17	9 + 9 = 18\t\n";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.content().string(expectTableString));
    }
}
